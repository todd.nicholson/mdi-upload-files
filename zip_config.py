# =============================================================================
#
# In order for this extractor to run according to your preferences,
# the following parameters need to be set.
#
# Some parameters can be left with the default values provided here - in that
# case it is important to verify that the default value is appropriate to
# your system. It is especially important to verify that paths to files and
# software applications are valid in your system.
#
# =============================================================================

# name to show in rabbitmq queue list
extractorName = "zip.collection.create"

# URL to be used for connecting to rabbitmq
rabbitmqURL = "amqp://guest:guest@127.0.0.1:5672/%2f"

# name of rabbitmq exchange
rabbitmqExchange = "clowder"

clowderhost = "127.0.0.1"
clowderport = "9000"

playserverKey = "phuong-test"
key_4ceed = "f019bf44-24df-4a99-9f4f-4527ef8bdff2"

# type of files to process
messageType = "*.zip.collection.create.#"

# trust certificates, set this to false for self signed certificates
sslVerify=False
