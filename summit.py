import logging
import time
import pyclowder.extractors as extractors
import zip_helper_2
import requests
import pika
import json

clowder_url = 'http://ec2-13-59-26-72.us-east-2.compute.amazonaws.com:9000/'
key = 'phoung-test'
zach_id = '596e602c14f32bd258ad2d64'

def testURL():
    url = clowder_url+'api/mdi/test'
    r = requests.get(url, stream=True, verify=False)
    r.raise_for_status()
    print("done")

def downloadFile(collectionName, authorEmail):
    url = clowder_url+'api/mdi/sendGlobus'
    print('url',url)
    parameters = dict()
    parameters['collectionName'] = collectionName
    parameters['authorEmail'] = authorEmail
    r = requests.get(url, params=parameters, verify=False)
    r.raise_for_status()
    if (r.status_code == requests.codes.ok):
        print("200")
        asjson = r.json()
        return str(asjson)
    else:
        return None

def downloadDatasetURL(datasetId):
    url = clowder_url+'api/datasets/'+datasetId+'/download'
    return url

def makeZachAuthor(currentId, objectType):
    secret_key = key
    if (objectType) == 'dataset':
        requestURL = clowder_url + 'api/collections/' +currentId + '/changeOwnerDataset/' + zach_id + "?key=" + secret_key
        # headers = {'Content-type': 'application/json'}
        # body = {'rootId': col_id, 'newUserId': new_author_id, 'methodKey': zip_config.key_4ceed}
        r = requests.post(requestURL)
        return r
    if (objectType) == 'file':
        requestURL = clowder_url + 'api/collections/' + currentId + '/changeOwnerFile/' + zach_id + "?key=" + secret_key
        # headers = {'Content-type': 'application/json'}
        # body = {'rootId': col_id, 'newUserId': new_author_id, 'methodKey': zip_config.key_4ceed}
        r = requests.post(requestURL)
        return r



def uploadFiles(list_of_files,list_of_metadata,dataset_name):

    datasetId = zip_helper_2.postDataset(dataset_name, "")
    makeZachAuthor(datasetId,'dataset')
    if len(list_of_files) == len(list_of_metadata):
        for i in range(0,len(list_of_files)):
            print('do stuff')
            current_file = list_of_files[i]
            current_metadata_json = list_of_metadata[i]
            file_id = zip_helper_2.postFileToDataset(current_file,datasetId)
            makeZachAuthor(file_id,'file')
            print("id of file is",str(file_id))
            try:
                url = clowder_url + 'api/files/' + file_id + '/metadata?key=' + key
                headers = {'Content-Type': 'application/json'}
                r = requests.post(url, headers=headers, data=current_metadata_json, verify=False)
                r.raise_for_status()
            except:
                print("error posting metadata")
    print("id of dataset created is", datasetId)
    return datasetId